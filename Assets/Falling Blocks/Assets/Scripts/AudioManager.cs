﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    //------------------------CREDITS----------------------------
    //Background music by Eric Matyas: http://www.soundimage.org
    //Sound effects: https://www.noiseforfun.com
    //-----------------------------------------------------------

    [SerializeField]
    private AudioSource backgroundMusic, scoreSound, deathSound, buttonClickSound;
    [SerializeField]
    private AudioSource[] scoreSounds;

    [HideInInspector]
    public bool soundIsOn = true;       //GameManager script might modify this value

    private int tempScoreSound = 0;

    public static AudioManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    //Functions are called by other scripts when it is necessary

    public void StopBackgroundMusic()
    {
        backgroundMusic.Stop();
    }

    public void PlayBackgroundMusic()
    {
        if (soundIsOn)
            backgroundMusic.Play();
    }

    public void ScoreSound()
    {
        if (soundIsOn)
        {
            scoreSounds[tempScoreSound].Play();

            if (tempScoreSound < scoreSounds.Length -1)
                tempScoreSound++;
            else
                tempScoreSound = 0;
        }
    }

    public void DeathSound()
    {
        if (soundIsOn)
            deathSound.Play();
    }

    public void ButtonClickSound()
    {
        if (soundIsOn)
            buttonClickSound.Play();
    }
}
