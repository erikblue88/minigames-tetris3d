﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public int spawnAtBeginning = 11;
    public GameObject[] obstacles;

    private int spawnType = 0, spawnedObstacles = 0;

    public static Spawner Instance;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        for (int i = 0; i < spawnAtBeginning; i++)      //Spawns in the beginning x times
            Spawn();
    }

    public void Spawn()
    {
        //Decides whether or not to increase obstacle type
        if ((spawnedObstacles >= spawnType + 3) && (spawnType < obstacles.Length - 1))
        {
            spawnType++;
            spawnedObstacles = 0;
        }

        Instantiate(obstacles[spawnType], transform.position, Quaternion.identity);     //Spawns selected obstacle to the position of the gameobject

        transform.position = new Vector3(transform.position.x, transform.position.y - 1f, transform.position.z);        //Moves the gameobject down
        spawnedObstacles++;
    }
}
