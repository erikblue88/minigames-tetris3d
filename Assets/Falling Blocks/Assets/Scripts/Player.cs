﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameObject shadow, block;
    public int degrees = 45;
    public float speedOfBlock, increaseSpeedBy = 0.15f;
    
    [HideInInspector]
    public int tempObstacleIndex = 0;

    public static Player Instance;

    private int tempObstacleCubeCount = 4;
    private float playerStartHeight;
    private bool dropped = true, canRotate = false;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        playerStartHeight = transform.position.y;
        Invoke("SpawnBlocksToPositions", 1f);       //First spawn after x seconds
    }

    public void RotateLeft()
    {
        if (canRotate)
            transform.Rotate(Vector3.up, degrees);
    }

    public void RotateRight()
    {
        if (canRotate)
            transform.Rotate(Vector3.up, -degrees);
    }

    public void DropBlock(bool canDrop = false)
    {
        if (!dropped || canDrop)
        {
            canRotate = false;
            dropped = true;
            StopAllCoroutines();
            StartCoroutine(Drop(Obstacle.obstacles[tempObstacleIndex].destroyedPositions[0].y));
        }
    }

    IEnumerator Move()
    {
        while (true)
        {
            transform.Translate(Vector3.down * speedOfBlock * Time.deltaTime);      //Moves gameobject down
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator Drop(float valueOnY = 1f)
    {
        StopCoroutine(Move());
        while (transform.position.y > valueOnY)
        {
            transform.Translate(Vector3.down * 10f * Time.deltaTime);       //Moves gameobject down faster
            yield return new WaitForEndOfFrame();
        }
        transform.position = new Vector3(transform.position.x, valueOnY, transform.position.z);

        EndMovement();
    }

    public void EndMovement()
    {
        dropped = true;
        for (int i = 0; i < transform.childCount; i++)
            Destroy(transform.GetChild(i).gameObject);

        speedOfBlock += increaseSpeedBy;
        Obstacle.obstacles[tempObstacleIndex].DestroyObstacle();
        transform.position = new Vector3(transform.position.x, transform.position.y + playerStartHeight, transform.position.z);
        tempObstacleIndex++;
        Invoke("SpawnBlocksToPositions", 1f);

        if (tempObstacleCubeCount < Obstacle.obstacles[tempObstacleIndex].transform.childCount)
        {
            tempObstacleCubeCount = Obstacle.obstacles[tempObstacleIndex].transform.childCount;
            CameraMovement.Instance.StartCoroutine(CameraMovement.Instance.Move(true));
        }
        else
            CameraMovement.Instance.StartCoroutine(CameraMovement.Instance.Move());

        StopAllCoroutines();
        canRotate = true;
    }

    public void SpawnBlocksToPositions()
    {
        StopAllCoroutines();
        canRotate = false;

        List<Obstacle> obstaclesList = Obstacle.obstacles;
        List<Vector3> destroyedPositions = obstaclesList[tempObstacleIndex].destroyedPositions;

        if (obstaclesList.Count > tempObstacleIndex)
        {
            for (int i = 0; i < destroyedPositions.Count; i++)
            {
                GameObject tempBlock = Instantiate(block, transform);
                tempBlock.transform.localPosition = new Vector3(destroyedPositions[i].x, 0f, destroyedPositions[i].z);
                tempBlock.tag = "PlayerBlock";
                tempBlock.GetComponent<MeshRenderer>().material.color = obstaclesList[tempObstacleIndex].transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().material.color;
                tempBlock.GetComponent<Animation>().Play();

                GameObject tempShadow = Instantiate(shadow, transform);

                tempShadow.GetComponent<ShadowPosition>().AddPositions(destroyedPositions[i], destroyedPositions[i].y + 0.51f);
                tempShadow.GetComponent<Collider>().enabled = (i == 0);
            }
        }

        transform.Rotate(Vector3.up * degrees * Random.Range(1, 360 / degrees));        //Rotates gameobject on the Y by a random value

        StartCoroutine(Move());
        canRotate = true;
        dropped = false;
    }

    public void StopBlocks()
    {
        bool placedCorrectly = false;
        List<Vector3> goalPositions = Obstacle.obstacles[tempObstacleIndex].destroyedPositions;

        #region ChecksIfCubesArePlacedCorrectly

        foreach (Transform block in transform)
        {
            if (block.gameObject.CompareTag("PlayerBlock"))
            {
                placedCorrectly = false;

                for (int i = 0; i < goalPositions.Count; i++)
                {

                    if ((Mathf.Abs(block.position.x - goalPositions[i].x) < 0.02f) && (Mathf.Abs(block.position.z - goalPositions[i].z) < 0.02f))
                    {
                        placedCorrectly = true;
                        break;
                    }
                }

                if (placedCorrectly == false)
                    break;
            }
            else
            {
                block.gameObject.SetActive(false);
            }
        }

        #endregion

        if (placedCorrectly)        //Can continue
        {
            ScoreManager.Instance.IncrementScore();
            Spawner.Instance.Spawn();
        }
        else      //Game is over
        {
            StopAllCoroutines();
            GameManager.Instance.EndPanelActivation();
        }
    }
}
