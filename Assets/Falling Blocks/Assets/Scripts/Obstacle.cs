﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    [HideInInspector]
    public List<Vector3> destroyedPositions = new List<Vector3>();

    public static List<Obstacle> obstacles = new List<Obstacle>();

    public static Color color;
    public static Vector2 increasingColor;

    private float tempColorChange = 0.07f;

    void Start()
    {
        obstacles.Add(this);        //Adds obstacle to the list

        #region Changing Color

        if (obstacles.Count == 1)
        {
            int randomIndex = Random.Range(0, 3);

            color = new Color(1f, 1f, 1f);
            color[randomIndex] = 0.5f;

            switch (randomIndex)
            {
                case 0:
                    increasingColor = new Vector2(1, -1);
                    break;
                case 1:
                    increasingColor = new Vector2(2, -1);
                    break;
                case 2:
                    increasingColor = new Vector2(0, -1);
                    break;
            }
        }

        if(obstacles.Count == 1)
        for (int i = 0; i < Random.Range(0, 10); i++)
        {
                color[(int)increasingColor[0]] += increasingColor[1] * tempColorChange;

                if (color[(int)increasingColor[0]] > 0.95f)
                {
                    color[(int)increasingColor[0]] = 1f;

                    if (0 == increasingColor[0])
                    {
                        if (color[1] == 1f)
                            increasingColor[0] = 1;
                        else
                            increasingColor[0] = 2;
                    }
                    else if (1 == increasingColor[0])
                    {
                        if (color[0] == 1f)
                            increasingColor[0] = 0;
                        else
                            increasingColor[0] = 2;
                    }
                    else
                    {
                        if (color[0] == 1f)
                            increasingColor[0] = 0;
                        else
                            increasingColor[0] = 1;
                    }
                    increasingColor[1] = -1;
                }
                else if (color[(int)increasingColor[0]] < 0.55f)
                {
                    color[(int)increasingColor[0]] = 0.5f;

                    if (0 == increasingColor[0])
                    {
                        if (color[1] == 0.5f)
                            increasingColor[0] = 1;
                        else
                            increasingColor[0] = 2;
                    }
                    else if (1 == increasingColor[0])
                    {
                        if (color[0] == 0.5f)
                            increasingColor[0] = 0;
                        else
                            increasingColor[0] = 2;
                    }
                    else
                    {
                        if (color[0] == 0.5f)
                            increasingColor[0] = 0;
                        else
                            increasingColor[0] = 1;
                    }
                    increasingColor[1] = 1;
                }

                foreach (Transform child in transform)
                    child.gameObject.GetComponent<MeshRenderer>().material.color = color;
            }

        color[(int)increasingColor[0]] += increasingColor[1] * tempColorChange;

        if (color[(int)increasingColor[0]] > 0.95f)
        {
            color[(int)increasingColor[0]] = 1f;

            if (0 == increasingColor[0])
            {
                if (color[1] == 1f)
                    increasingColor[0] = 1;
                else
                    increasingColor[0] = 2;
            }
            else if (1 == increasingColor[0])
            {
                if (color[0] == 1f)
                    increasingColor[0] = 0;
                else
                    increasingColor[0] = 2;
            }
            else
            {
                if (color[0] == 1f)
                    increasingColor[0] = 0;
                else
                    increasingColor[0] = 1;
            }
            increasingColor[1] = -1;
        }
        else if (color[(int)increasingColor[0]] < 0.55f)
        {
            color[(int)increasingColor[0]] = 0.5f;

            if (0 == increasingColor[0])
            {
                if (color[1] == 0.5f)
                    increasingColor[0] = 1;
                else
                    increasingColor[0] = 2;
            }
            else if (1 == increasingColor[0])
            {
                if (color[0] == 0.5f)
                    increasingColor[0] = 0;
                else
                    increasingColor[0] = 2;
            }
            else
            {
                if (color[0] == 0.5f)
                    increasingColor[0] = 0;
                else
                    increasingColor[0] = 1;
            }
            increasingColor[1] = 1;
        }

        foreach (Transform child in transform)
            child.gameObject.GetComponent<MeshRenderer>().material.color = color;

        #endregion

        //Selects random children and disables them
        for (int i = 0; i < Random.Range(transform.childCount / 4, (transform.childCount / 2) + 1); i++)
        {
            int randomIndex = Random.Range(0, transform.childCount);
            destroyedPositions.Add(transform.GetChild(randomIndex).position);
            transform.GetChild(randomIndex).gameObject.SetActive(false);
        }
    }

    public void DestroyObstacle()
    {
        //Enables children
        for (int i = 0; i < transform.childCount; i++)
            transform.GetChild(i).gameObject.SetActive(true);

        GetComponent<Animation>().Play();
        Invoke("DisableObstacles", 1f);
    }

    private void DisableObstacles()
    {
        //Disables children
        for (int i = 0; i < transform.childCount; i++)
            transform.GetChild(i).gameObject.SetActive(false);
    }
}
