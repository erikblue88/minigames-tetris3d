﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public float speed = 1f;

    public static CameraMovement Instance;

    private Camera cam;
    private Transform background;

    private void Awake()
    {
        //Initializations
        background = transform.GetChild(0);
        cam = GetComponent<Camera>();
        Instance = this;
    }

    public IEnumerator Move(bool canChangeSize = false)
    {
        float posY = transform.position.y - 0.75f;      //Next position

        while (transform.position.y > posY)     //If gameobject has not reached the next position yet
        {
            transform.Translate(Vector3.down * speed * Time.deltaTime);     //Moves the gameobject
            yield return new WaitForEndOfFrame();
        }

        transform.position = new Vector3(transform.position.x, posY, transform.position.z);

        if (canChangeSize)
            StartCoroutine(ChangeSize());
    }

    public IEnumerator ChangeSize()
    {
        float newSize = cam.orthographicSize + 1f;

        foreach (Transform child in transform)
            child.localScale = new Vector2(child.localScale.x * 1.2f, child.localScale.y * 1.2f);       //Scales the children

        while (cam.orthographicSize < newSize)
        {
            cam.orthographicSize += 0.025f;     //Changes camera size
            yield return new WaitForEndOfFrame();
        }
    }
}
