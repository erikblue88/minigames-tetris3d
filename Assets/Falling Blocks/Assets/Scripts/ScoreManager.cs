﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour {

    public TextMeshPro scoreText;

    [HideInInspector]
    public int score = 0;

    public static ScoreManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    public void IncrementScore()
    {
        if (GameManager.Instance.gameIsOver == false)       //If the game is not over
            scoreText.text = (++score).ToString();      //Increments the 'scoretext' text as well as the score variable's value and writes it out to the screen
        AudioManager.Instance.ScoreSound();      //Plays scoreSound
    }
}
