﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowPosition : MonoBehaviour
{
    private Vector3 startPos;
    private float positionY;
    private bool positionsAreSet = false;
    private Transform playerTransform, blockTransform;
    private Renderer rend;
    private Color newColor;
    private float startDistanceFromPlayer;

    private void Start()
    {
        //Initializations
        newColor = Color.black;
        playerTransform = transform.parent.transform;
        blockTransform = playerTransform.GetChild(transform.GetSiblingIndex() - 1);
        startDistanceFromPlayer = Vector3.Distance(playerTransform.position, transform.position);
        rend = GetComponent<Renderer>();
    }

    void Update()
    {
        if (positionsAreSet)
            transform.position = new Vector3(blockTransform.position.x, positionY, blockTransform.position.z);      //Position is fixed on the Y axis

        //Changes color
        newColor.a = 1f - (Vector3.Distance(playerTransform.position, transform.position) / startDistanceFromPlayer);
        rend.material.color = newColor;
    }

    public void AddPositions(Vector3 startPosition, float posY)
    {
        startPos = startPosition;
        positionY = posY;
        positionsAreSet = true;
        transform.position = new Vector3(startPosition.x, positionY, startPosition.z);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PlayerBlock"))        //If gameobject collides with "PlayerBlock"
        {
            Player.Instance.DropBlock(true);
            Player.Instance.StopBlocks();
            gameObject.SetActive(false);
        }
    }
}
