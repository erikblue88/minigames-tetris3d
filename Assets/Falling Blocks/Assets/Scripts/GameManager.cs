﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    //----------------------------------------------
    //Thank you for purchasing the asset! If you have any questions/suggestions, don't hesitate to contact me!
    //E-mail: ragendom@gmail.com
    //Please let me know your impressions about the asset by leaving a review, I will appreciate it.
    //----------------------------------------------

    public GameObject startPanel, endPanel, pausedPanel, pauseButton, muteImage, reviveButton, gameButtons;
    public TextMeshProUGUI highScoreText, endScoreText, endHighScoreText;
    public TextMeshPro scoreText;

    [HideInInspector]
    public bool gameIsOver = false;

    public static GameManager Instance;

    private void Awake()
    {
        Instance = this;
        Obstacle.obstacles.Clear();
    }

    void Start () {
        //UNCOMMENT THE FOLLOWING LINES IF YOU ENABLED UNITY ADS AT UNITY SERVICES AND REOPENED THE PROJECT!
        //if (AdManager.Instance.unityAds)
        //    CallUnityAds();     //Calls Unity Ads
        //else
        CallAdmobAds();     //Calls Admob Ads

        StartPanelActivation();
        HighScoreCheck();
        AudioCheck();
    }

    //UNCOMMENT THE FOLLOWING LINES IF YOU ENABLED UNITY ADS AT UNITY SERVICES AND REOPENED THE PROJECT!
    //public void CallUnityAds()
    //{
    //    if (Time.time != Time.timeSinceLevelLoad)
    //        AdManager.Instance.ShowUnityVideoAd();      //Shows Interstitial Ad when game starts (except for the first time)
    //    AdManager.Instance.HideAdmobBanner();
    //}

    public void CallAdmobAds()
    {
        AdManager.Instance.ShowAdmobBanner();        //Shows Banner Ad when game starts
        if (Time.time != Time.timeSinceLevelLoad)
            AdManager.Instance.ShowAdmobInterstitial();      //Shows Interstitial Ad when game starts (except for the first time)
    }

    public void Initialize()
    {
        gameButtons.SetActive(false);
        scoreText.enabled = false;
        pauseButton.SetActive(false);
    }

    public void StartPanelActivation()
    {
        Initialize();
        startPanel.SetActive(true);
        endPanel.SetActive(false);
        pausedPanel.SetActive(false);
    }

    public void EndPanelActivation()
    {
        gameIsOver = true;
        gameButtons.SetActive(false);
        AudioManager.Instance.DeathSound();
        startPanel.SetActive(false);
        endPanel.SetActive(true);
        pausedPanel.SetActive(false);
        scoreText.enabled = false;
        endScoreText.text = scoreText.text;
        pauseButton.SetActive(false);
        HighScoreCheck();
    }

    public void PausedPanelActivation()
    {
        startPanel.SetActive(false);
        endPanel.SetActive(false);
        pausedPanel.SetActive(true);
    }

    public void HighScoreCheck()
    {
        if (ScoreManager.Instance.score > PlayerPrefs.GetInt("HighScore", 0))
        {
            PlayerPrefs.SetInt("HighScore", ScoreManager.Instance.score);
        }
        highScoreText.text = "RECORD " + PlayerPrefs.GetInt("HighScore", 0).ToString();
        endHighScoreText.text = "RECORD " + PlayerPrefs.GetInt("HighScore", 0).ToString();
    }

    public void AudioCheck()
    {
        if (PlayerPrefs.GetInt("Audio", 0) == 0)
        {
            muteImage.SetActive(false);
            AudioManager.Instance.soundIsOn = true;
            AudioManager.Instance.PlayBackgroundMusic();
        }
        else
        {
            muteImage.SetActive(true);
            AudioManager.Instance.soundIsOn = false;
            AudioManager.Instance.StopBackgroundMusic();
        }
    }

    public void StartButton()
    {
        gameButtons.SetActive(true);
        pauseButton.SetActive(true);
        scoreText.enabled = true;
        startPanel.SetActive(false);
        AudioManager.Instance.ButtonClickSound();
        Player.Instance.enabled = true;
    }

    public void RestartButton()
    {
        AudioManager.Instance.ButtonClickSound();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void AudioButton()
    {
        AudioManager.Instance.ButtonClickSound();
        if (PlayerPrefs.GetInt("Audio", 0) == 0)
            PlayerPrefs.SetInt("Audio", 1);
        else
            PlayerPrefs.SetInt("Audio", 0);
        AudioCheck();
    }

    public void PauseButton()
    {
        pauseButton.SetActive(false);
        gameButtons.SetActive(false);
        PausedPanelActivation();
        scoreText.enabled = false;
        AudioManager.Instance.StopBackgroundMusic();
        Time.timeScale = 0f;
    }

    public void ResumeButton()
    {
        Time.timeScale = 1f;
        gameButtons.SetActive(true);
        AudioManager.Instance.PlayBackgroundMusic();
        scoreText.enabled = true;
        pauseButton.SetActive(true);
        pausedPanel.SetActive(false);
    }

    public void HomeButton()
    {
        ResumeButton();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Revive()
    {
        //UNCOMMENT THE FOLLOWING LINES IF YOU ENABLED UNITY ADS AT UNITY SERVICES AND REOPENED THE PROJECT!
        //if (AdManager.Instance.unityAds)
        //    AdManager.Instance.ShowUnityRewardVideoAd();       //Shows Unity Reward Video ad
        //else
        AdManager.Instance.ShowAdmobRewardVideo();       //Shows Admob Reward Video ad

        endPanel.SetActive(false);
        reviveButton.SetActive(false);
        pauseButton.SetActive(true);
        scoreText.enabled = true;
        gameButtons.SetActive(true);

        Player.Instance.EndMovement();

        gameIsOver = false;
    }
}
